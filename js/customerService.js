﻿(function () {
    var app = angular.module('mclApp');
    app.service('CustomerService', [function () {
        this.students = [
            {
                name: 'Naruto',
                snumber: '2007107101',
                age: 21,
                birthdate: '01/30/2014'
            },
            {
                name: 'Sasuke',
                snumber: '2014107101',
                age: 21,
                birthdate: '05/22/2014'
            }
        ];

        this.currentStudent = {
            name: '',
            snumber: '',
            age: '',
            birthdate: ''
        };

        this.addStudent = function (student) {

            if (student.name && student.snumber && student.age && student.birthdate) {
                for (var i = 0; i < this.students.length; i++) {
                    if (this.students[i].snumber == student.snumber)
                        return false;
                }
                this.students.push(student);
                return true;
            }
            return false;
        }

        this.getStudents = function () {
            return this.students;
        };

        this.removeStudent = function (snumber) {
            for (var i = 0; i < this.students.length; i++) {
                if (this.students[i].snumber == snumber) {
                    this.students.splice(i, 1);
                }
            }
        };

        this.saveStudent = function (student) {
            for (var i = 0; i < this.students.length; i++) {
                if (this.students[i].snumber == student.snumber) {
                    this.students[i] = student;
                }
            }
        };

        this.getSelectedStudent = function () {
            return this.currentStudent;
        };

        this.setSelectedStudent = function (student) {
            student.birthdate = new Date(student.birthdate);
            this.currentStudent = student;
        };
    }
    ]);
})();