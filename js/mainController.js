﻿(function () {
    var app = angular.module('mclApp');
    app.controller('MainController', ['$location', 'CustomerService', function ($location, customerService) {

        this.hasError = false;
        this.isSuccessful = false;
        this.students = initList();
        this.student = initStudent();

        function initList() {
            return customerService.getStudents();
        };

        function initStudent() {
            return {
                name: '',
                snumber: '',
                age: '',
                birthdate: ''
            }
        };

        this.add = function () {
            var result = customerService.addStudent(this.student);
            if (result) {
                this.hasError = false;
                this.students = initList();
                this.student = initStudent();
                this.isSuccessful = true;
            }
            else {
                this.hasError = true;
                this.isSuccessful = false;
            }
        };

        this.edit = function (student) {
            customerService.setSelectedStudent(student);
            $location.path('/edit');
        };

        this.remove = function (snumber) {
            customerService.removeStudent(snumber);
        };

    }
    ]);
})();