﻿(function () {
    var app = angular.module('mclApp');
    app.controller('EditController', ['CustomerService', function (customerService) {

        this.student = initStudent();

        function initStudent() {
            return customerService.getSelectedStudent();
        };

        this.save = function (student) {
            customerService.saveStudent(student);
        };

    }
    ]);
})();