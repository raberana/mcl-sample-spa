﻿(function () {
    var app = angular.module('mclApp', ['ngRoute']);

    app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: 'partials/main.html',
                controller: 'MainController as mCtrl'
            })
            .when('/edit', {
                templateUrl: 'partials/edit.html',
                controller: 'EditController as eCtrl'
            })
            .otherwise({
                templateUrl: 'partials/main.html',
                controller: 'MainController as mCtrl'
            });
    }]);

})();